{-# LANGUAGE GADTs #-}

import Prelude hiding (Maybe, Nothing, Just)

-- Algebras -- a recap

-- Given an endofunctor f, we can define Eilenberg-Moore algebras associated to it:
data ExprF x = ValF Int | PlusF x x | MultF x x

instance Functor ExprF where
  fmap f (ValF n) = ValF n
  fmap f (PlusF a b) = PlusF (f a) (f b)
  fmap f (MultF a b) = MultF (f a) (f b)

-- An Eilenberg-Moore algebra is an object c (carrier) together with a morphism
-- f c -> c (structure map)
type Algebra f c = f c -> c

-- The following are two Eilenberg-Moore algebras with different carriers (one
-- with carrier Int, and another with carrier String)
eval :: Algebra ExprF Int
eval (ValF n) = n
eval (PlusF n m) = n + m
eval (MultF n m) = n * m

pretty :: Algebra ExprF String
pretty (ValF n) = show n
pretty (PlusF s t) = s ++ " + " ++ t
pretty (MultF s t) = s ++ "*" ++ t

-- A morphism of Eilenberg-Moore algebras (c,h) -> (c',h') is a morphism f:c->c' s.t.
-- Fc ------------> Fc'
-- |       Ff        |
-- |h                |h'
-- |                 |
-- ˇ        f        ˇ
-- c --------------> c'
-- This makes the Eilenberg-Moore algebras and their morphisms into a category.
-- The initial object of this category is called the initial algebra.
-- Let (i,iota) be the initial algebra for the Eilenberg-Moore category induced
-- by f.
-- It can be shown that Fi=~i, and iota has an inverse.
-- So an initial algebra is the solution of the equation Fi=i
-- In Haskell we denote the initial algebra over f as Fix f
data Fix f where
  -- Here In is the structure morphism of the initial algebra
  In :: f (Fix f) -> Fix f

-- Fix f is in a sense a sort of recursive limit of f. The point where applying
-- f one more time doesn't change anything. In our ExprF example this would be
-- the type of all expressions one can construct using ValF and PlusF.
-- Note that ExprF( ExprF a) is either of the form (ValF n) or (PlusF e1 e2)
-- where e1 and e2 are of type ExprF a.

-- And out is the inverse of In:
out :: Fix f -> f (Fix f)
out (In x) = x

-- We can define terms of type Fix ExprF by using so called "smart constructors"
val :: Int -> Fix ExprF
val n = In (ValF n)

plus :: Fix ExprF -> Fix ExprF -> Fix ExprF
plus e1 e2 = In (PlusF e1 e2)

mult :: Fix ExprF -> Fix ExprF -> Fix ExprF
mult e1 e2 = In (MultF e1 e2)

-- For example:
e3 :: Fix ExprF
e3 = mult (plus (val 2) (val 3)) (val 4)

-- We want to perform computations on recursive data structures.
-- We are going to use the fact that Fix f is initial to do that.
cata :: Functor f => Algebra f a -> Fix f -> a
cata alg = alg . fmap (cata alg) . out

-- consider 'cata eval e9'
-- So what this does is the following: suppose we apply cata alg to e3 :: Fix ExprF.
-- Now every term of type Fix ExprF is of the form In e4. So suppose in particular
-- e3 = In $ PlusF (plus (val 2) (val 3)) (val 4)
-- Next we fmap cata eval onto PlusF (plus (val 2) (val 3)) (val 4)
-- so we get PlusF (cata eval $ plus (val 3) (val 4)) (cata eval $ val 4)
-- Then we apply eval:
-- eval $ PlusF (cata eval $ plus (val 2) (val 3)) (cata eval $ val 4)
-- Next we look at (cata eval $ val 4)
-- First we write val 4 as In (ValF 4), then we apply out to get (ValF 4), 
-- then we fmap cata eval onto it which is just 4
-- On the other branch: cata eval $ plus (val 2) (val 3)
-- plus (val 2) (val 3) is In (PlusF (val 2) (val 3))
-- so we fmap cata eval onto PlusF (val 2) (val 3) to get
-- PlusF (cata eval $ val 2) (cata eval $ val 3) and apply eval:
-- eval $ PlusF (cata eval $ val 2) (cata eval $ val 3)
-- We have seen that (cata eval $ val n) is just n, so the previous line is:
-- eval $ PlusF 2 3, which is 2+3=5
-- Conjoining it with the second branch:
-- eval $ PlusF 5 4=9

-- We define Maybe
data Maybe x = Nothing | Just x

-- Then make it into a functor
instance Functor Maybe where
  fmap f Nothing = Nothing
  fmap f (Just x) = Just $ f x

-- Then define an F-algebra structure over Int
algMaybe :: Algebra Maybe Int
algMaybe Nothing = 23
algMaybe (Just x) = x

-- Then we need to figure out what Fix Maybe might be
-- It has to be independent of the parameter type a
-- If we think of it as the limit Maybe^n a, then it is just Nothing wrapped in
-- arbitrary number of Just's
-- Hence it is basically Nat

semmi :: Maybe x -> Fix Maybe
semmi Nothing = In Nothing

bele :: Fix Maybe -> Fix Maybe
bele x = In $ Just x

e1 = bele $ bele $ bele $ semmi Nothing

-- So in the usual list fold, the folding function and the starting acc
-- together constitute the algebra structure
