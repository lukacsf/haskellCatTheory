import Prelude hiding (Monad, (<=<), return, (>>=))
-- Monads

-- The monad in Haskell is basically the Kleisli category of the monad.
class Functor m => Monad m where
  -- This corresponds to the Kleisli-composition, which makes use of the
  -- multiplication of the monad
  (<=<) :: (b -> m c) -> (a -> m b) -> (a -> m c)
  -- This is the unit of the adjunction, which incidentally also has as
  -- components the identity arrows of the Kleisli category.
  return :: a -> m a

instance Monad Maybe where
  return = Just
  f <=< g = \a -> case g a of
    Nothing -> Nothing
    Just b  -> f b

-- Mathematicians' definition
class Functor m => Monad' m where
  -- Multiplication:
  multiplication :: m (m a) -> m a
  -- Unit:
  unit :: a -> m a

instance Monad' Maybe where
  multiplication Nothing = Nothing
  multiplication (Just Nothing) = Nothing 
  multiplication (Just (Just x)) = Just x
  unit = Just

-- The 'fish operator' of this version follows the mathematical version of
-- Kleisli-composition verbatim.
(<=<<) :: Monad' m => (b -> m c) -> (a -> m b) -> (a -> m c)
g <=<< f = multiplication . fmap g . f

-- Programmers' definition
class Functor m => Monad'' m where
  (>>=) :: m a -> (a -> m b) -> m b
  return'' :: a -> m a

instance Monad'' Maybe where
  Nothing >>= g = Nothing
  (Just x) >>= g = g x
  return'' = Just

(<<=<<) :: Monad'' m => (b -> m c) -> (a -> m b) -> (a -> m c)
g <<=<< f = \x -> f x >>= g

-- Note that one could also implement join for Monad''
join'' :: Monad'' m => m (m a) -> m a
join'' mma = mma >>= id
