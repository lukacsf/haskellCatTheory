{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE GADTs #-}

import Control.Exception
import Data.Functor.Const as C

-- Free Monads

data FreeMonad f a where
  -- This is just putting a value into the free monad
  Pure :: a -> FreeMonad f a
  Free :: f (FreeMonad f a) -> FreeMonad f a

instance Functor f => Functor (FreeMonad f) where
  fmap g (Pure x) = Pure $ g x
  -- Okay, so fmap g  is just applying the functor FreeMonad f to the morphism g.
  -- i.e. fmap g :: FreeMonad f a -> FreeMonad f b
  -- So we can fmap this to get
  -- fmap (fmap g) :: f (FreeMonad f a) -> f (FreeMonad f b)
  -- And we can get rid of the outer f by applying Free
  fmap g (Free ffx) = Free $ fmap (fmap g) ffx

instance Functor f => Applicative (FreeMonad f) where
  pure = Pure

  -- (<*>) :: m (a -> b) -> m a -> m b

  -- Pure g :: FreeMonad f (a -> b) [Hence g :: a -> b]
  -- Free ffx :: FreeMonad f a [Hence ffx :: f (FreeMonad f a)]
  Pure g <*> fx = fmap g fx
  -- fx :: FreeMonad f a
  -- (<*> fx) :: FreeMonad f (a -> b) -> FreeMonad f b
  -- fmap (<*> fx) :: f (FreeMonad f (a -> b)) -> f (FreeMonad f b)
  -- fmap (<*> fx) ffg :: f (FreeMonad f b)
  -- free $ fmap (<*> fx) ffg :: FreeMonad f b
  Free ffg <*> fx = Free $ fmap (<*> fx) ffg

instance Functor f => Monad (FreeMonad f) where
  Pure x >>= g = g x
  (Free ffx) >>= g = Free $ fmap (>>= g) ffx

-- We can also implement the monadic multiplication
mu :: Functor f => FreeMonad f (FreeMonad f a) -> FreeMonad f a
mu (Pure fa) = fa
-- ffa :: f (FreeMonad f (FreeMonad f a))
-- fmap mu :: f (FreeMonad f (FreeMonad f a)) -> f (FreeMonad f a)
-- fmap mu ffa :: f (FreeMonad f a)
-- Free $ fmap mu ffa
mu (Free ffa) = Free $ fmap mu ffa

-- Example from:
-- https://www.haskellforall.com/2012/06/you-could-have-invented-free-monads.html
-- The idea is that we have a very simple programming language that has the
-- following commands:
data Toy b next = Done | Output b next | Bell next

-- We want to create an interpreter for this language.

-- First we have to fing a way to compose these commands.
-- That's where monads come into play.

-- from here on the functor is (Toy b)
instance Functor (Toy b) where
  fmap f Done = Done
  fmap f (Bell x) = Bell $ f x
  fmap f (Output y x) = Output y $ f x

-- Given that (Toy b) is now a functor, we already have the monad instance for
-- the free monad induced by it. So we can convert our commands to monadic
-- values, so the might be composed, and represented in Haskell.
output :: a -> FreeMonad (Toy a) ()
output x = Free $ Output x (Pure ())

bell :: FreeMonad (Toy a) ()
bell = Free $ Bell (Pure ())

done :: FreeMonad (Toy a) r
done = Free Done

-- This is a subrutine of a program written in our language, represented in Haskell
subrutine :: FreeMonad (Toy Char) ()
subrutine = output 'A'

-- This is how the subrutine fits together with other parts of the program.
program :: FreeMonad (Toy Char) r
program = do
  subrutine
  bell
  done

-- Okay, now that we can represent the syntax tree of our programming language
-- in Haskell, it is time to write an interpreter that interprets the syntax
-- tree.

-- Our first interpreter is going to be a pretty printer
showProgram :: (Show a, Show r) => FreeMonad (Toy a) r -> String
showProgram (Free (Output x y)) = "output " ++ show x ++ "\n" ++ showProgram y
showProgram (Free (Bell y)) = "bell\n" ++ showProgram y
showProgram (Free Done) = "done!\n"
showProgram (Pure y) = "return " ++ show y ++ "\n"

pretty :: (Show a, Show r) => FreeMonad (Toy a) r -> IO ()
pretty = putStr . showProgram

-- Now let's write an actual interpreter
interpret :: (Show a) => FreeMonad (Toy a) r -> IO ()
interpret (Free (Output x prog)) = print x >> interpret prog
interpret (Free (Bell prog)) = print "BIMBAM" >> interpret prog
interpret (Free Done) = return ()
interpret (Pure x) = throwIO (userError "Improper termination")

main :: IO ()
main = interpret program

-- Back to The Dao of Functional Programming
-- To interpret a free monad value, we need an algebra in the category of
-- endofunctors.
type MAlg f g a = (a -> g a, f (g a) -> g a)

-- Technically, this whole thing is the same as the algebra/catamorphism stuff,
-- just one step of abstraction higher.
-- So we want to define the 'monad catamorphism'
mcata :: Functor f => MAlg f g a -> FreeMonad f a -> g a
mcata (l, r) (Pure a) = l a
mcata (l, r) (Free ffa) = r $ fmap (mcata (l, r)) ffa

-- Let us rewrite now the interpreter from the previous example in this style
interpretProgram :: Show a => MAlg (Toy a) IO ()
interpretProgram = (error, interp)
  where
    error x = throwIO (userError "Improper termination")
    -- interp :: Toy a (IO ()) -> IO ()
    interp (Output x io) = print x >> io
    interp (Bell io) = print "BIMBAM" >> io
    interp Done = return ()

prettyPrint :: Show a => MAlg (Toy a) IO String
prettyPrint = (ret, pr)
  where
    ret x = return $ "return " ++ show x ++ " --> "
    -- pr :: Toy a (IO String) -> IO String
    pr (Output x io) = do
      prog <- io
      return $ "output: " ++ show x ++ " --> " ++ prog
    pr (Bell io) = do
      prog <- io
      return $ "bell --> " ++ prog
    pr Done = return "done!"

runProgram :: Show a => FreeMonad (Toy a) () -> IO ()
runProgram = mcata interpretProgram

printProgram :: Show a => FreeMonad (Toy a) String -> IO String
printProgram = mcata prettyPrint

program2 :: FreeMonad (Toy Char) r
program2 = do
  output 'X'
  output 'P'
  bell
  output 'Z'
  bell
  done

-- Exercise 14.7.1.
data Rose a = Leaf a | Rose [Rose a]
  deriving (Functor)

-- What is FreeMonad [] a?
-- Pure :: a -> FreeMonad [] a
-- Free :: [FreeMonad [] a] -> Freemonad [] a

rtof :: Rose a -> FreeMonad [] a
rtof (Leaf x) = Pure x
rtof (Rose (r : rs)) = Free $ rtof r : fmap rtof rs

ftor :: FreeMonad [] a -> Rose a
ftor (Pure x) = Leaf x
ftor (Free (x : xs)) = Rose $ ftor x : fmap ftor xs

-- Exercise 14.7.2.
data Bin a = Bin a a
  deriving (Functor)

-- First, what is FreeMonad Bin a?
-- Pure :: a -> FreeMonad Bin a
-- Free :: Bin (FreeMonad Bin a) (FreeMonad Bin a) -> FreeMonad Bin a

data Tree a = Level a | Node (Tree a) (Tree a)

btof :: Tree a -> FreeMonad Bin a
btof (Level a) = Pure a
btof (Node left right) = Free $ Bin (btof left) (btof right)

ftob :: FreeMonad Bin a -> Tree a
ftob (Pure a) = Level a
ftob (Free (Bin left right)) = Node (ftob left) (ftob right)

-- Stack Calculator
data StackF k
  = Push Int k
  | Top (Int -> k)
  | Pop k
  | Add k
  deriving (Functor)

-- This is similar to what I did previously based on the article I linked.
liftF :: (Functor f) => f r -> FreeMonad f r
liftF fr = Free $ fmap Pure fr

-- Smart constructors
push :: Int -> FreeMonad StackF ()
push n = liftF (Push n ())

pop :: FreeMonad StackF ()
pop = liftF (Pop ())

top :: FreeMonad StackF Int
top = liftF (Top id)

add :: FreeMonad StackF ()
add = liftF (Add ())

-- Now we can use do notation to create calculations
calc :: FreeMonad StackF Int
calc = do
  push 3
  push 4
  add
  x <- top
  pop
  return x

-- This type specifies how state transitions work
newtype StackAction k = St ([Int] -> ([Int], k))
  deriving (Functor)

runAction :: StackAction k -> [Int] -> ([Int], k)
runAction (St k) = k

runAlg :: MAlg StackF StackAction a
runAlg = (stop, go)
  where
    stop :: a -> StackAction a
    stop x = St (\ns -> (ns, x))
    go :: StackF (StackAction a) -> StackAction a
    go (Pop k) = St (runAction k . tail)
    go (Top ik) = St (\ns -> runAction (ik $ head ns) ns)
    go (Push n k) = St (\ns -> runAction k (n : ns))
    go (Add k) = St (\(x:y:ns) -> runAction k (x+y:ns))

runCalc :: FreeMonad StackF k -> ([Int], k)
runCalc program = runAction (mcata runAlg program) []

-- Exercise 14.7.4
showAlg :: MAlg StackF (Const String) a
showAlg = (l, r)
  where
    l :: a -> Const String a
    l x = Const "stop\n"
    r :: StackF (Const String a) -> Const String a
    r (Pop k) = Const "pop\n"
    r (Add k) = Const "add\n"
    r (Top ik) = Const "choose branch\n"
    r (Push n k) = Const $ "push " ++ show n ++ " to the stack\n"


