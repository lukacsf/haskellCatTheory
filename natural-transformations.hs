{-# LANGUAGE GADTs #-}
{-# LANGUAGE KindSignatures #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE TypeFamilies #-}

import Data.Bifunctor
import Data.Functor.Contravariant
import Data.Kind
import Data.Void
import Prelude hiding (id)

-- Natural Transformations

-- A natural transformation is a family of morphisms parametrised by the
-- objects of a category. In programming this would be a polymorphic function.

data MyNatural :: (Type -> Type) -> (Type -> Type) -> Type where
  MyNatural :: (forall a. f a -> g a) -> MyNatural f g

-- Types defined by forall are parametric polymorphic, that is, they are
-- defined by the same formula for every type. This is as opposed to ad-hoc
-- polymorphism, where every type might have a different implementation.
-- Parametric polymorphic function:
id :: forall a. a -> a
id x = x

-- fmap is an ad-hoc polymorphic function.

-- the type keyword is used to create what is basically a type alias: the RHS
-- can be substituted for RHS at any point in the code
type Natural f g = forall a. f a -> g a

safeHead :: Natural [] Maybe
safeHead [] = Nothing
safeHead (x : xs) = Just x

myReverse :: Natural [] []
myReverse [] = []
myReverse (x : xs) = myReverse xs ++ [x]

-- This is very slow, because when appending an element to the end of the list,
-- the list has to traversed every time.

-- The function objects were defined by the unique existence of certain arrows:
-- x*a
--  |\
--  |  \
--  |h*id\f
--  |      \
--  ˇ        \
--  b^a*a --> b
--        ev
-- That is, for any f:x*a -> b, there exists a unique h: x -> b^a s.t. ev . (h*id) =f
-- Phrased differently, there is a natural bijection between morphisms x*a -> b and
-- x -> b^a.

-- And hence the natural isomorphism C(-*a,b) -> C(-,b^a)
-- In Haskell, this natural isomorphism is just `curry` and its inverse `uncurry`.
-- Taking x=b^a, we get C(b^a*a,b^a) -> C(b^a,b^a), so for h=id*id, f=ev. But
-- under the inverse of the natural isomorphism, f is the image of id. Hence in
-- Haskell:
apply :: (a -> b, a) -> b
apply = uncurry id -- uncurry is the inverse of the natural iso

-- We can also implement the two functors C(-*a,b) and (-,b^a)
newtype LeftFunctor a b x = LF ((x, a) -> b)

newtype RightFunctor a b x = RF (x -> (a -> b))

-- These are both contravariant functors:
instance Contravariant (LeftFunctor a b) where
  -- We have f:x'->x and g:x*a->b. First we create h=f*id: x'*a->x*a.
  -- In Haskell, that is h= bimap f id
  -- And then we apply g to get g . f*id: x'*a->x*a->b
  contramap f (LF g) = LF (g . bimap f id)

instance Contravariant (RightFunctor a b) where
  -- This is easier: g: x -> b^a, f: x' -> x, so we just take g.f:x'->x->b^a
  contramap f (RF g) = RF (g . f)

-- We can now also implement the natural isomorphism encapsulated in oue nice
-- categorical formalism
alpha :: forall a b. Natural (LeftFunctor a b) (RightFunctor a b)
alpha (LF f) = RF (curry f)

-- and its inverse
alpha_1 :: forall a b. Natural (RightFunctor a b) (LeftFunctor a b)
alpha_1 (RF g) = LF (uncurry g)

-- The Yoneda lemma says that [C,set](C(a,-),F)=Fa. 
-- We implement explicitly the isomorphism.
yoneda :: (Functor f) => Natural ((->)a) f -> f a
yoneda alpha = alpha id

yoneda_1 :: (Functor f) => f a -> Natural ((->)a) f
-- yoneda^-1_x: C(a,x) -> Fx, so given h:a->x, we can apply F to get Fh:Fa->Fx
-- and we can evaluate Fh at p: (Fh)(p) is in Fx. So we can map h to (Fh)(p)
yoneda_1 p = \h -> fmap h p

-- The Yoneda lemma has a contravariant version as well: [C,set](C(-,a),F)=Fa
-- Note that here F is contravariant!
coyoneda :: (Functor f) => (forall x. (x -> a) -> f x) -> f a
coyoneda alpha = alpha id

coyoneda_1 :: (Contravariant f) => f a -> (forall x. (x -> a) -> f x)
-- The idea is the same: given h:x->a, we want an element of Fx. So we apply F
-- (contravariant!) to get Fh:Fa->Fx and evaluate it at p
coyoneda_1 p = \h -> contramap h p

-- A functor F:C->set is representable if it is isomorphic to C(x,-) for some
-- object x in C
class Representable f where
  -- This is essentially the representing object (type)
  type Key f :: Type
  -- Let x be the representing object (type; x=Key f)
  -- Here Key f -> a is just C(x,a). Tabulate maps it to f a.
  tabulate :: (Key f -> a) -> f a
  -- This is basically the inverse of tabulate
  index :: f a -> (Key f -> a)
  -- Hence index and tabulate are just the natural isomorphism establishing the
  -- representability of f and its inverse.

-- Think of streams as infinite products.
data Stream a = Stm a (Stream a)
-- Hence they are indexed by the natural numbers:
data Nat where
  Z :: Nat
  S :: Nat -> Nat

instance Representable Stream where
  type Key Stream = Nat
  tabulate g = tab Z
    where
      -- tab :: Nat -> Stream a
      tab n = Stm (g n) (tab (S n))
  index (Stm element _) Z = element
  -- If we are looking for the n+1st element of the stream, then we remove the
  -- first element, and look at the nth element of the rest of the stream
  index (Stm _ rest) (S n) = index rest n


-- Exercise 9.9.2: another example of a representable functor
data Pair x = Pair x x

instance Representable Pair where
  type Key Pair = Bool
  tabulate g = Pair (g True) (g False)
  index (Pair x y) = \bool -> if bool then x else y

-- Exercise 9.9.3: consider the functor that maps every type to the terminal type
data Unit a = U

instance Representable Unit where
  type Key Unit = Void
  -- Recall that absurd is the unique function from the initial object (type)
  tabulate absurd = U
  index U = absurd

-- Exercise 9.9.4: The identity functor is representable, and list can be
-- considered a sum of the identity function suitably many times.
newtype Identity a = Id a

instance Representable Identity where
  type Key Identity = ()
  tabulate g = Id $ g ()
  index (Id x) = \() -> x
