{-# LANGUAGE GADTs #-}

-- We want to experiment with defininf standard types
import Prelude hiding (Either, Left, Right, Maybe, Just, Nothing)

-- Sum Types

-- Simplest type: () (unit) the terminal object of \Hask.
-- There is only one term of type () also denoted as ().
-- Terms of types can be thought of as arrows from ().
someTerm :: () -> a
someTerm = undefined

-- The Bool type is the sum of two instances of ()
data MyBool where
  MyTrue :: MyBool
  MyFalse :: MyBool

-- Every function from Bool to A is equivalent to two terms of type A
-- The following two function give the isomorphisms
-- If functions () -> a correspond to terms of type a, then by the universal
-- property of the sum, functions () + () -> a correspond pairs of terms of
-- type a
boolElimination :: (MyBool -> a) -> (a, a)
boolElimination f = (f MyTrue, f MyFalse)

boolIntrofuction :: (a, a) -> MyBool -> a
boolIntrofuction (x, _) MyTrue = x
boolIntrofuction (_, y) MyFalse = y

-- We can add () even more times:
data RGB = Red | Green | Blue
-- These are basically type constructors with 0 parameters

-- A term of type RGB can have values 'Red', 'Green', 'Blue', e.g.
rgbTerm :: RGB
rgbTerm = Green

-- Similarly to the Bool case, functions from RGB to a correspond to triples of
-- elements of a
-- The following functions witness the isomorphism. Notice that they are
-- inverses to each other
rgbElimination :: (RGB -> a) -> (a, a, a)
rgbElimination f = (f Red, f Green, f Blue)

rgbIntroduction :: (a, a, a) -> RGB -> a
rgbIntroduction (x, y, z) rgb = case rgb of
  Red -> x
  Green -> y
  Blue -> z

-- The type 'Char' can be thought of as one giant sum of as many instances of
-- () as there are unicode characters, with constructors the characters
-- themselves between two apostrophes
myChar :: Char
myChar = 'f'

-- \Hask also has an initial object, Void. There is exactly one function 
-- Void -> a for every type a, called absurd.
data Void where

absurd :: Void -> a
absurd v = undefined
-- Since  there are no terms of type Void (it kind of corresponds to the empty
-- set), it can never happen that absurd is evaluated. So we can just leave it
-- undefined

-- We can also form the sum of types other than ()
-- This construction is called Either in Haskell
data Either a b where
  Left :: a -> Either a b
  Right :: b -> Either a b

-- Notice that in category theory, sum is a limit cone a -> a+b <- b, where the
-- two canonical morphisms are exactly Left and Right.
-- The universal property of sum states that functions a + b -> c correspond to
-- a pair of funtions a -> c, b -> c.
sumElimination :: (Either a b -> c) -> (a -> c, b -> c)
sumElimination f = (g, h)
  where
    g = f . Left
    h = f . Right

sumIntroduction :: (a -> c, b -> c) -> Either a b -> c
sumIntroduction (f, g) either = case either of
  Left x -> f x
  Right y -> g y

-- Hence the general way to construct a function Either a b -> c is the
-- following:
functionFromSum :: Either a b -> c
functionFromSum (Left x) = f x
  where
    f :: a -> c
    f = undefined -- Whatever you want it to be
functionFromSum (Right y) = g y
  where
    g :: b -> c
    g = undefined 

-- There is a particularly useful standard sum type, Maybe. It is used to model
-- computations that might fail, with Nothing representing the failed
-- computation.

-- Notice that it has a type parameter: if the copmutation does not fail, the
-- result has to have a type, and we need to specify that in advance, for
-- Haskell is strictly typed.
data Maybe a = Nothing | Just a

-- The empty list does not have a first element, so this function might fail.
firstElement :: [a] -> Maybe a
firstElement [] = Nothing
firstElement (a:_) = Just a

-- To see that forming sum types is commutative and associative, we can use the
-- introduction and elimination rules. By Yoneda's lemma we know that two types
-- are isomorphic, if the set \Hask(a,c)=\Hask(b,c) for every third type c.

-- So for associativity, note that functions (a+b)+c -> d correspond to triples
-- of functions. But functions of type a+(b+c) -> d do as well. Hence they must
-- be isomorphic. Similarly for commutativity.

-- Functoriality:
sumLiftFunction :: (a -> a') -> (b -> b') -> Either a b -> Either a' b'
sumLiftFunction f g c = case c of
  Left x -> Left $ f x
  Right y -> Right $ g y
