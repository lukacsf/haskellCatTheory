{-# LANGUAGE GADTs #-}
{-# LANGUAGE TupleSections #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE RankNTypes #-}

import Prelude hiding (Monad, (<=<), (>>=))
-- Monads

-- We are going to use the mathematician's definition
class Functor m => Monad m where
  unit :: a -> m a
  multiplication :: m (m a) -> m a

(<=<) :: Monad m => (b -> m c) -> (a -> m b) -> (a -> m c)
g <=< f = multiplication . fmap g . f

(>>=) :: Monad m => m a -> (a -> m b) -> m b
ma >>= f = multiplication . fmap f $ ma

-- Partiality
instance Monad Maybe where
  unit = Just
  multiplication Nothing = Nothing
  multiplication (Just Nothing) = Nothing
  multiplication (Just (Just x)) = Just x

-- Logging
newtype Writer w a = Writer (a, w)

instance Functor (Writer w) where
  fmap f (Writer (a, w)) = Writer (f a, w)

instance Monoid w => Monad (Writer w) where
  unit a = Writer (a, mempty)
  multiplication (Writer (Writer (a, w), w')) = Writer (a, mappend w w')

-- Environment
newtype Reader e a = Reader (e -> a)

runReader :: Reader e a -> e -> a
runReader (Reader h) = h

instance Functor (Reader e) where
  fmap f (Reader g) = Reader $ f . g

instance Monad (Reader e) where
  unit = Reader . const
  -- Reader e (Reader e a): e -> e -> a
  multiplication (Reader h) = Reader $ \e -> runReader (h e) e

-- Exercise 14.4.1.
newtype E e a = E (e -> Maybe a)

instance Functor (E e) where
  fmap f (E h) = E $ fmap f . h

instance Monad (E e) where
  unit = E . const . Just
  -- E e (E e a) : e -> Maybe (e -> Maybe a) => e -> Maybe a
  multiplication (E h) = E $ \e -> case h e of
    Nothing -> Nothing
    Just (E g)  -> g e

-- State

newtype State s a = State (s -> (a, s))

runState :: State s a -> s -> (a, s)
runState (State h) = h

instance Functor (State s) where
  fmap f (State h) = State $ \s -> let (x, s') = h s in (f x, s')

instance Monad (State s) where
  unit x = State $ \s -> (x, s)
  -- State s (State s a): s -> (s -> (a, s), s)
  multiplication (State h) = State $ \s -> let (h', s') = h s in runState h' s'

-- Nondeterminism
instance Monad [] where
  unit x = [x]
  multiplication [] = []
  multiplication (x : xs) = x ++ multiplication xs

-- Constinuation
newtype Cont r a = Cont ((a -> r) -> r)

runCont :: Cont r a -> (a -> r) -> r
runCont (Cont h) = h

instance Functor (Cont r) where
  fmap atob (Cont h ) = Cont $ \btor -> h $ btor . atob

instance Monad (Cont r) where
  unit x = Cont $ \ator -> ator x
  -- Cont r (Cont r a): (((a -> r) -> r) -> r) -> r
  -- (Cont r a -> r) -> r
  -- For this specific runCont :: 
  multiplication (Cont h) = Cont $ \ator -> h (\(Cont h') -> h' ator)
  -- The argument to h should be a function (Cont r a -> r).
  -- We define it to map (Cont h') (here h':(a->r)->r) to its value on ator

-- Exercise 14.5.2
pairs :: [a] -> [b] -> [(a, b)]
pairs as bs = as >>= \a -> bs >>= \b -> [(a, b)]

-- Exaclty the same but with do notation:
pairsDo :: [a] -> [b] -> [(a, b)]
pairsDo as bs = do
  a <- as
  b <- bs
  return (a, b)

-- Exercise 14.5.1
-- This solution works, it's just that the do notation does not work with my
-- shady homemade implementation of the monad class
ap :: Monad m => m (a -> b) -> m a -> m b
ap mf mx = mx >>= \x -> mf >>= \f -> unit (f x)
-- ap mf mx = do
--   f <- mf
--   x <- mx
--   return $ f x


-- Substitution
data Ex x = Val Int | Var x | Plus (Ex x) (Ex x) deriving Show

instance Functor Ex where
 fmap f (Val n) = Val n
 fmap f (Var x) = Var $ f x
 fmap f (Plus e1 e2) = Plus (fmap f e1) (fmap f e2)

-- (2+a)+b
ex :: Ex Char
ex = Plus (Plus (Val 2) (Var 'a')) (Var 'b')

instance Monad Ex where
  unit x = Var x
  multiplication (Val n) = Val n
  multiplication (Var x) = x
  multiplication (Plus e1 e2) = Plus (multiplication e1) (multiplication e2)

sub :: Char -> Ex String
sub 'a' = Plus (Var "x1") (Val 2)
sub 'b' = Var "x2"

ex' :: Ex String
ex' = ex >>= sub


-- Free Monads
-- We construct free monoids in the category of endofunctors.
-- Recall that the free monoid in Hask was the fixed point of the list functor
-- Fx=1+a*x
-- Similarly, we now have L=Id+(FL)
-- which is equivalent to Id -> L and F*L -> L by the universal property of the sum
data FreeMonad f a where
  -- Id -> L
  Pure :: a -> FreeMonad f a
  -- F*L -> L
  Free :: f (FreeMonad f a) -> FreeMonad f a

instance Functor f => Functor (FreeMonad f) where
  fmap f (Pure a) = Pure $ f a
  fmap f (Free ffa) = Free (fmap (fmap f) ffa)

instance Functor f => Monad (FreeMonad f) where
  unit = Pure
  multiplication (Pure fa) = fa
  multiplication (Free ffa) = Free $ fmap multiplication ffa

class Monoidal f where
  monUnit :: f ()
  (>*<) :: forall a b. f a -> f b -> f (a, b)

instance Monoidal [] where
  monUnit = []
  la >*< lb = zip la lb

-- Strength for an endofunctor is sigma : a*Fb -> F(a*b)
strength :: Functor f => (e, f a) -> f (e, a)
strength (e, as) = fmap (e, ) as

-- In Haskell every functor is strong and every monad is monoidal:
