{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE TypeApplications #-}

import Data.List

class Functor w => Comonad w where
  extract :: w a -> a
  duplicate :: w a -> w (w a)
  extend :: (w a -> b) -> w a -> w b
  extend f wa = f <$> duplicate wa
  (==<==) :: (w b -> c) -> (w a -> b) -> (w a -> c)
  g ==<== f = g . fmap f . duplicate

-- The Stream comonad
data Stream a = Cons a (Stream a) deriving (Functor)

instance Comonad Stream where
  extract (Cons s str) = s
  duplicate (Cons s str) = Cons (Cons s str) (duplicate str)

avg :: Stream Double -> Double
avg = (/ 5) . sum . stmTake 5

stmTake :: Int -> Stream a -> [a]
stmTake 0 _ = []
stmTake n (Cons s str) = s : stmTake (n -1) str

smooth :: Stream Double -> Stream Double
smooth = extend avg

data Signal a = Sig (Double -> a) Double deriving (Functor)

instance Comonad Signal where
  extract (Sig f x) = f x

  -- Signal (Signal a) = Sig (Double -> Signal a) Double
  -- Sig (Double -> Sig (a -> Double) Double) Double
  duplicate (Sig f x) = Sig (\y -> Sig f (x - y)) x

-- Exercise 16.1.2.
data BiStream a = BStr [a] [a] deriving (Functor)

instance Comonad BiStream where
  extract (BStr past future) = head future

  -- BiStream (BiStream a) = [BiStream a] [BiStream a]
  duplicate (BStr past present) = BStr dupPast dupPast
    where
      dupPast = zipWith BStr (tails past) (tails present)

-- Comonoid
-- Of course, a comonad is a comonoid in the category of endofunctors
class Comonoid w where
  split :: w -> (w, w)
  destroy :: w -> ()

instance Comonoid w where
  split w = (w, w)
  destroy w = ()

-- Costate comonad
-- Consider the currying adjunction: La = (a,s), Ra = s->a
-- This generates the state monad : RLa = s->(a,s)
-- We'll now take a look at the comonad it generates: LRs=(s->a,s)

data Store s c where
  St :: (s -> c) -> s -> Store s c

instance Functor (Store s) where
  fmap f (St h s) = St (f . h) s

instance Comonad (Store s) where
  extract (St h s) = h s

  -- Store s (Store s a) = (s -> Store s a) s = (s -> St h s) s
  duplicate (St h s) = St (St h) s

-- The counit of this comonad is precisely extract.
-- To compute the comultiplication, let us first consider the unit of the adjunction
eta' :: c -> (s -> (c, s))
eta' x = \s -> (x, s)

-- Then the comultiplication is L eta R
-- To compute this, first look at a simplified example in which we take s to be Int.
-- Then the left functor becomes
newtype Pair c = P (c, Int) deriving (Functor)

-- And the right functor (as a type synonym)
type Fun c = Int -> c

-- The unit of the adjunction between these functors can then be written as
eta :: forall c. c -> Fun (Pair c)
eta c = \s -> P (c, s)

-- This lets us implement the comultiplication as the whiskering of eta
comult :: forall c. Pair (Fun c) -> Pair (Fun (Pair (Fun c)))
-- The right whiskering is done automatically, and the left is just lifting,
-- which can be done by fmap
comult = fmap @Pair eta

-- comult (P (f, s)) = P (\s' -> P (f, s), s)

-- Cellular automaton

-- A cell can either be live or dead
data Cell = L | D deriving (Show)

-- This is the coKleisli arrow implementing rule 110
step :: Store Int Cell -> Cell
step (St f n) = case (f (n -1), f n, f (n + 1)) of
  (L, L, L) -> D
  (L, D, D) -> D
  (D, D, D) -> D
  _ -> L

-- Lenses
-- Consider the coalgebra for the Store monad with carrier s given by
phi' :: s -> Store a s
phi' x = St (set x) (get x)

-- This is equivalent to
set :: s -> a -> s
set = undefined

-- and
get :: s -> a
get = undefined

-- Such a pair is called a lens, with s being the source and a the focus.

-- For simplicity's sake we get rid of all the data constructors. Then the
-- abova simplifies to
phi :: s -> (a -> s, s)
phi s = (set s, get s)

epsilon :: (a -> s, a) -> s
epsilon (f, a) = f a

delta :: (a -> s, a) -> (a -> (a -> s, a), a)
delta (f, a) = (\x -> (f, x), a)

-- The first law of comonad coalgebras tells us that
-- set s (get s) = s
-- etc.
